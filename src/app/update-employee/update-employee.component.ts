import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {
  employee:Employee;
  id:number;
  constructor(private employeeService:EmployeeService,private route:ActivatedRoute,
    private router:Router) { }

  ngOnInit(): void {
    this.employee=new Employee();
    this.id = this.route.snapshot.params['id'];
    this.employeeService.getAnEmployee(this.id).subscribe(data=>
      {
        this.employee=data;
      })
      
  }
  onSubmit()
  {
    this.employeeService.updateEmployee(this.id,this.employee).subscribe(data=>console.log(data));
    this.gotoList();
  }
  gotoList()
  {
    this.router.navigate(['employees']);
  }

}
