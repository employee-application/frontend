import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { Employee } from './employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {



  constructor(private httpClient:HttpClient) { }
  
  getEmployees()  {
    return this.httpClient.get<Employee[]>(`http://localhost:8080/api/v1/employees`);
  }

  saveEmployee(employee:Employee)
  {
    return this.httpClient.post<any>(`http://localhost:8080/api/v1/add`,employee);
  }

  getAnEmployee(id:number)
  {
    return this.httpClient.get<Employee>(`http://localhost:8080/api/v1/get/${id}`);
  }

  updateEmployee(id:number,employee:Employee)
  {
    return this.httpClient.put<any>(`http://localhost:8080/api/v1/update/${id}`,employee);
  }
  deleteEmployee(id:number)
  {
    return this.httpClient.delete(`http://localhost:8080/api/v1/delete/${id}`);
  }
  
}
