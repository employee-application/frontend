export class Employee {
    id: number;
    fname: string;
    lname: string;
    emailId: string;
}
