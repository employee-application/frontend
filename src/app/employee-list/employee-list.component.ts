import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employees:Employee[]
  
  constructor(private employeeService:EmployeeService,private router:Router) { }

  ngOnInit(): void {
    this.getEmployeeList();
  }
  private getEmployeeList() {
    this.employeeService.getEmployees().subscribe(employees => this.employees = employees);
  }
  updateEmployee(id:number)
  {
    this.router.navigate(['update-employee',id])
  }
  clickMethod(id:number) {
    if(confirm("Are you sure to delete the Employee ? ")) {
      this.deleteEmployee(id);
    }
    
  }

  deleteEmployee(id:number)
  {
    this.employeeService.deleteEmployee(id).subscribe(data=>
      {
        console.log(data);
        this.getEmployeeList();
      })
  }
  viewEmployee(id:number)
  {
    this.router.navigate(['view-employee',id]);
  }

}
