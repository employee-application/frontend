import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
  styleUrls: ['./view-employee.component.css']
})
export class ViewEmployeeComponent implements OnInit {

  constructor(private route:ActivatedRoute,private employeeService:EmployeeService) { }
  id:number;
  employee:Employee;

  ngOnInit(): void 
  {
    this.id=this.route.snapshot.params['id'];
    this.employeeService.getAnEmployee(this.id).subscribe(data=>
      {
        this.employee=data;
      })
  }

}
